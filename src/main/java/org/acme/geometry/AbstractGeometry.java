package org.acme.geometry;

public abstract class AbstractGeometry implements Geometry{

    public abstract Geometry clone();
    public String asText(){
        WktVisitor visitor = new WktVisitor();
        // accepte le visitor et va appeler le visit correspondants
        this.accept(visitor);
        return visitor.getResult();
    }

    public Envelope getEnvelope(){
        EnvelopeBuilder builder = new EnvelopeBuilder();
        this.accept(builder);
        return builder.build();
    }
}
