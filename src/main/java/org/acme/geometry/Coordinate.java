package org.acme.geometry;

public class Coordinate {
    double x;
    double y;

    public Coordinate(){
        this.x = Double.NaN;
        this.y = Double.NaN;
    }
    public Coordinate(double x, double y) {
        this.x = x;
        this.y = y;
    }
    public boolean isEmpty(){
        return Double.isNaN(x);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
