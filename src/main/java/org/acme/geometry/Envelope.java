package org.acme.geometry;

public class Envelope {
    Coordinate bottomLeft;
    Coordinate topRight;
    public Envelope() {
        this.bottomLeft = new Coordinate();
        this.topRight = new Coordinate();
    }

    public Envelope(Coordinate bottomLeft, Coordinate topRight) {
        this.bottomLeft = bottomLeft;
        this.topRight = topRight;
    }

    public boolean isEmpty(){
        return this.bottomLeft.isEmpty() || this.topRight.isEmpty();
    }

    public Double getXmin(){
        return this.bottomLeft.getX();
    }
    public Double getYmin(){
        return this.bottomLeft.getY();
    }
    public Double getXmax(){
        return this.topRight.getX();
    }
    public Double getYmax(){
        return this.topRight.getY();
    }

}
