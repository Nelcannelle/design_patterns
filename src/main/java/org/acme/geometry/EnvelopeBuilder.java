package org.acme.geometry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EnvelopeBuilder implements GeometryVisitor {
    List<Double> xVals;
    List<Double> yVals;

    public EnvelopeBuilder() {
        this.xVals = new ArrayList<Double>();
        this.yVals = new ArrayList<Double>();
    }

    public void insert(Coordinate coordinate) {
        this.xVals.add(coordinate.getX());
        this.yVals.add(coordinate.getY());

    }
    public Envelope build(){
        Coordinate topRight = new Coordinate(Collections.max(xVals), Collections.max(yVals));
        Coordinate bottomLeft = new Coordinate(Collections.min(xVals), Collections.min(yVals));
        return new Envelope(bottomLeft, topRight);
    }

    @Override
    public void visit(Point point) {
        this.insert(point.coordinate);
    }

    @Override
    public void visit(Linestring linestring) {
        for(int i =0; i< linestring.getNumPoints();i++){
            this.insert(linestring.getPointN(i).coordinate);
        }
    }
}
