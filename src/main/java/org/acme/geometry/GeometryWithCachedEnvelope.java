package org.acme.geometry;

public class GeometryWithCachedEnvelope implements Geometry{
    Geometry original;
    Envelope cachedEnvelope;

    public GeometryWithCachedEnvelope(Geometry original) {
        this.original = original;
        this.cachedEnvelope = new Envelope();
    }


    @Override
    public String getType() {
        return this.getType();
    }

    @Override
    public boolean isEmpty() {
        return this.isEmpty();
    }

    @Override
    public void translate(double dx, double dy) {
        this.original.translate(dx,dy);
    }

    @Override
    public Geometry clone() {
        return this.clone();
    }

    @Override
    public Envelope getEnvelope() {
        if (this.cachedEnvelope.isEmpty()){
            this.cachedEnvelope = this.original.getEnvelope();
        }
        return this.cachedEnvelope;
    }

    @Override
    public void accept(GeometryVisitor visitor) {
        
    }

    @Override
    public String asText() {
        return this.asText();
    }
}
