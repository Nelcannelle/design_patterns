package org.acme.geometry;

import java.util.ArrayList;
import java.util.List;

public class Linestring  extends AbstractGeometry{
    List<Point> points;

    public Linestring() {
        points = new ArrayList<Point>();
    }

    public Linestring(List<Point> points) {
        this.points = points;
    }

    public Point getPointN(int n) {
        return points.get(n);
    }
    public int getNumPoints() {
        return points.size();
    }
    public String getType(){
        return "LineString";
    }

    @Override
    public boolean isEmpty() {
        return this.points.isEmpty();
    }

    @Override
    public void translate(double dx, double dy) {
        for(Point p: points){
            p.translate(dx,dy);
        }
    }

    @Override
    public Linestring clone() {
        List<Point> l = new ArrayList<Point>();
        for(Point p : this.points){
            l.add(p.clone());
        }
        return new Linestring(l);

    }

    @Override
    public Envelope getEnvelope() {
        EnvelopeBuilder builder = new EnvelopeBuilder();
        for (Point p : this.points){
            builder.insert(p.getCoordinate());
        }
        return builder.build();
    }

    @Override
    public void accept(GeometryVisitor visitor) {
        visitor.visit(this);
    }



}
