package org.acme.geometry;

public class WktVisitor implements GeometryVisitor {
    StringBuilder buffer;

    public WktVisitor() {
        this.buffer = new StringBuilder();
    }

    @Override
    public void visit(Point point) {
        if (point.isEmpty()) {
            buffer.append("POINT EMPTY");
        }else{
            buffer.append("POINT("+ point.getCoordinate().getX() +" "+ point.getCoordinate().getY()+")");
        }
    }

    @Override
    public void visit(Linestring linestring) {
        if(linestring.isEmpty()){
            buffer.append("LINESTRING EMPTY");
        }else{
            buffer.append("LINESTRING(");
            for (int i =0; i<linestring.getNumPoints(); i++){
                if (i!=0){
                    buffer.append(',');
                }
                if (linestring.getPointN(i).isEmpty()){
                    buffer.append("NaN NaN");
                }else{
                    buffer.append(linestring.getPointN(i).getCoordinate().getX() +" " + linestring.getPointN(i).getCoordinate().getY());
                }
            }
            buffer.append(")");
        }
    }

    public String getResult(){
        return this.buffer.toString();
    }
}
