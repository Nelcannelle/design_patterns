package org.acme.geometry;

public class WktWriter {

    public String writer(Geometry geometry) {
        if ( geometry instanceof Point ){
            Point point = (Point)geometry;
            // traiter le cas Point
            if (point.isEmpty()) {
                return "POINT EMPTY";
            }else{
                return "POINT("+ point.getCoordinate().getX() +" "+ point.getCoordinate().getY()+")";
            }
        }else if ( geometry instanceof Linestring ){
            Linestring lineString = (Linestring)geometry;
            String s = "";
            if(lineString.isEmpty()){
                return "LINESTRING EMPTY";
            }else{
                // traiter le cas Linestring
                for (Point p : lineString.points){
                    s = s + p.getCoordinate().getX() +" " + p.getCoordinate().getY()+ ",";
                }
                return "LINESTRING("+ s.substring(0, s.length() - 1) +")";
            }
        }else{
            throw new RuntimeException("geometry type not supported");
        }
    }
}
