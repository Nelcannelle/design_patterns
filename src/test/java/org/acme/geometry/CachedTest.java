package org.acme.geometry;

import org.junit.Assert;
import org.junit.Test;

public class CachedTest {
    TestGeometryFactory tgf = new TestGeometryFactory();
    @Test
    public void testConstructorXY(){

        Point a = tgf.pointA();
        GeometryWithCachedEnvelope g = new GeometryWithCachedEnvelope(a);
        Envelope e = g.getEnvelope() ; // calcul et stockage dans cachedEnvelope
        Envelope b = g.getEnvelope() ; // renvoi de cachedEnvelope
        Assert.assertSame(e,b);
    }
}
