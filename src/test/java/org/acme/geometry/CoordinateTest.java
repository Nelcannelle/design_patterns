package org.acme.geometry;

import org.junit.Assert;
import org.junit.Test;

import static java.lang.Double.NaN;

public class CoordinateTest {
	TestGeometryFactory tgf = new TestGeometryFactory();
	public static final double EPSILON = 1.0e-15;

	@Test
	public void testDefaultConstructor(){

		Coordinate c = tgf.nullCoordinate();
		Assert.assertEquals(NaN, c.getX(), EPSILON);
		Assert.assertEquals(NaN, c.getY(), EPSILON);
	}
	@Test
	public void testConstructorXY(){

		Coordinate c = tgf.CoordinateA();
		Assert.assertEquals(3.0, c.getX(), EPSILON);
		Assert.assertEquals(3.0, c.getY(), EPSILON);
	}

	@Test
	public void testEmpty(){
		Coordinate c = tgf.nullCoordinate();
		Assert.assertEquals(true, c.isEmpty());
	}


}
