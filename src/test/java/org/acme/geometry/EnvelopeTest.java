package org.acme.geometry;

import org.junit.Assert;
import org.junit.Test;

import static java.lang.Double.NaN;

public class EnvelopeTest {

    TestGeometryFactory tgf = new TestGeometryFactory();
    public static final double EPSILON = 1.0e-15;
    @Test
    public void testEmpty(){
        Envelope envelope = tgf.nullEnvelope();
        Assert.assertEquals(true, envelope.isEmpty());
    }
    public void testDefaultConstructor(){
        Envelope e= tgf.nullEnvelope();
        Assert.assertEquals(NaN, e.getXmax(), EPSILON);
        Assert.assertEquals(NaN, e.getYmax(), EPSILON);
        Assert.assertEquals(NaN, e.getXmin(), EPSILON);
        Assert.assertEquals(NaN, e.getYmin(), EPSILON);
        Assert.assertTrue(e.isEmpty());

    }
    @Test
    public void testConstructor(){
        EnvelopeBuilder builder = new EnvelopeBuilder();
        builder.insert(new Coordinate(2.0,1.0));
        builder.insert(new Coordinate(17.0,16.0));
        Envelope e= builder.build();
        Assert.assertEquals(17.0, e.getXmax(), EPSILON);
        Assert.assertEquals(16.0, e.getYmax(), EPSILON);
        Assert.assertEquals(2.0, e.getXmin(), EPSILON);
        Assert.assertEquals(1.0, e.getYmin(), EPSILON);
    }
    @Test
    public void testCached(){
        Geometry g = tgf.pointA();
        g = new GeometryWithCachedEnvelope(g);
        Envelope a = g.getEnvelope() ; // calcul et stockage dans cachedEnvelope
        Envelope b = g.getEnvelope() ; // renvoi de cachedEnvelope
        Assert.assertSame(a,b);
    }

}
