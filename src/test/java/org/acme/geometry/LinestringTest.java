package org.acme.geometry;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class LinestringTest {
    TestGeometryFactory tgf = new TestGeometryFactory();
    public static final double EPSILON = 1.0e-15;
    @Test
    public void testDefaultConstructor(){
        Linestring l = tgf.nullLinestring();
        Assert.assertEquals(0, l.getNumPoints());
    }

    @Test
    public void testConstructorXY(){
        Linestring l = tgf.listLinestring();
        Envelope e = l.getEnvelope();
        Assert.assertEquals(2, l.getNumPoints());
        Assert.assertEquals(2.0, e.getYmin(), EPSILON);
    }
    @Test
    public void testType(){
        Linestring l = tgf.listLinestring();
        Assert.assertEquals("LineString", l.getType());
    }

    @Test
    public void testPointN(){
        Linestring l = tgf.listLinestring();
        Assert.assertEquals(2.0, l.getPointN(0).getCoordinate().getX(), EPSILON);
    }

    @Test
    public void testEmpty(){
        Linestring l = tgf.nullLinestring();
        Assert.assertEquals(true, l.isEmpty());
    }

    @Test
    public void testTranslate(){

        Point a = tgf.pointA();
        Point b = tgf.pointB();
        a.translate(2.0,2.0);
        b.translate(1.0,1.0);
        Assert.assertEquals(4.0, a.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(4.0, a.getCoordinate().getY(), EPSILON);
        Assert.assertEquals(18.0, b.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(18.0, b.getCoordinate().getY(), EPSILON);
    }

    @Test
    public void testClone(){
        Linestring l = tgf.listLinestring();
        Linestring cl = l.clone();
        cl.translate(1.0,1.0);
        Assert.assertEquals(2.0, l.getPointN(0).getCoordinate().getX(), EPSILON);
        Assert.assertEquals(3.0, cl.getPointN(0).getCoordinate().getY(), EPSILON);
    }

    @Test
    public void testConsoleNullLinestring() throws UnsupportedEncodingException {
        Linestring l = tgf.nullLinestring();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(os);
        LogGeometryVisitor visitor = new LogGeometryVisitor(out);
        l.accept(visitor);
        Assert.assertEquals("Je suis une polyligne définie par 0 point(s)", os.toString("UTF8").trim());
    }
    @Test
    public void testConsoleLinestring() throws UnsupportedEncodingException {
        Linestring l = tgf.listLinestring();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(os);
        LogGeometryVisitor visitor = new LogGeometryVisitor(out);
        l.accept(visitor);
        Assert.assertEquals("Je suis une polyligne définie par 2 point(s)", os.toString("UTF8").trim());
    }

    @Test
    public void testWktLinestring(){
        WktVisitor visitor = new WktVisitor();
        Geometry geometry = tgf.listLinestring();
        geometry.accept(visitor);
        Assert.assertEquals( "LINESTRING(2.0 2.0,17.0 17.0)", visitor.getResult());
        Assert.assertEquals( "LINESTRING(2.0 2.0,17.0 17.0)", geometry.asText());
    }

    @Test
    public void testWktNullLinestring(){
        WktVisitor visitor = new WktVisitor();
        Geometry geometry = tgf.nullLinestring();
        geometry.accept(visitor);
        Assert.assertEquals( "LINESTRING EMPTY", visitor.getResult() );
        Assert.assertEquals( "LINESTRING EMPTY", geometry.asText() );
    }
    @Test
    public void testWktLinestringAndNull(){
        WktVisitor visitor = new WktVisitor();
        Point a = tgf.pointA();
        Point n = tgf.nullPoint();
        ArrayList<Point> listeP = new ArrayList<>();
        listeP.add(a);
        listeP.add(n);
        Geometry geometry = new Linestring(listeP);
        geometry.accept(visitor);
        Assert.assertEquals( "LINESTRING(2.0 2.0,NaN NaN)", visitor.getResult() );
        Assert.assertEquals( "LINESTRING(2.0 2.0,NaN NaN)", geometry.asText() );
    }




}
