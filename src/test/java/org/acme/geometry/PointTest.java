package org.acme.geometry;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import static java.lang.Double.NaN;

public class PointTest {
    TestGeometryFactory tgf = new TestGeometryFactory();
    public static final double EPSILON = 1.0e-15;

    @Test
    public void testDefaultConstructor(){
        Point p = tgf.nullPoint();
        Envelope e = p.getEnvelope();
        Assert.assertEquals(NaN, p.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(NaN, p.getCoordinate().getY(), EPSILON);
        Assert.assertEquals(NaN, e.getYmin(), EPSILON);
    }

    @Test
    public void testConstructorXY(){
        Point a = tgf.pointA();
        Envelope e = a.getEnvelope();
        Assert.assertEquals(2.0, a.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(2.0, a.getCoordinate().getY(), EPSILON);
        Assert.assertEquals(2.0, e.getYmin(), EPSILON);
    }

    @Test
    public void testType(){
        Point a = tgf.pointA();
        Assert.assertEquals("Point", a.getType());
    }
    @Test
    public void testEmpty(){
        Point p = tgf.nullPoint();
        Assert.assertEquals(true, p.isEmpty());
    }

    @Test
    public void testTranslate(){
        Point p = tgf.pointA();
        p.translate(2.0,2.0);
        Assert.assertEquals(4.0, p.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(4.0, p.getCoordinate().getY(), EPSILON);
    }

    @Test
    public void testClone(){
        Point p = tgf.pointA();
        Point pc = p.clone();
        Assert.assertEquals(2.0, pc.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(2.0, pc.getCoordinate().getY(), EPSILON);
    }

    @Test
    public void testConsoleNullPoint() throws UnsupportedEncodingException {
        Point p = tgf.nullPoint();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(os);
        LogGeometryVisitor visitor = new LogGeometryVisitor(out);
        p.accept(visitor);
        Assert.assertEquals("Je suis un point avec x=NaN et y=NaN", os.toString("UTF8").trim());
    }
    @Test
    public void testConsolePointA() throws UnsupportedEncodingException {
        Point p = tgf.pointA();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(os);
        LogGeometryVisitor visitor = new LogGeometryVisitor(out);
        p.accept(visitor);
        Assert.assertEquals("Je suis un point avec x=2.0 et y=2.0", os.toString("UTF8").trim());
    }

    @Test
    public void testWktPointA(){
        WktVisitor visitor = new WktVisitor();
        Geometry geometry = tgf.pointA();
        geometry.accept(visitor);
        Assert.assertEquals( "POINT(2.0 2.0)", visitor.getResult() );
    }

    @Test
    public void testWktNullPoint(){
        WktVisitor visitor = new WktVisitor();
        Geometry geometry = tgf.nullPoint();
        geometry.accept(visitor);
        Assert.assertEquals( "POINT EMPTY", visitor.getResult() );
    }

    public void testAsTextPointA(){
        WktVisitor visitor = new WktVisitor();
        Geometry geometry = tgf.pointA();
        Assert.assertEquals( "POINT(2.0 2.0)", geometry.asText() );
    }

    @Test
    public void testAsTextNullPoint(){
        WktVisitor visitor = new WktVisitor();
        Geometry geometry = tgf.nullPoint();
        Assert.assertEquals( "POINT EMPTY", geometry.asText());
    }
}
