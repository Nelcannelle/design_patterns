package org.acme.geometry;

import java.util.ArrayList;

public class TestGeometryFactory {
    //Points
    public Point nullPoint(){
        Point p = new Point();
        return p;
    }
    public Point pointA(){
        Coordinate c = new Coordinate(2.0,2.0);
        Point p = new Point(c);
        return p;
    }
    public Point pointB(){
        Coordinate c = new Coordinate(17.0,17.0);
        Point p = new Point(c);
        return p;
    }

    //Coordinates
    public Coordinate nullCoordinate(){
        return new Coordinate();
    }
    public Coordinate CoordinateA(){
        return new Coordinate(3.0,3.0);
    }

    //Linestrings
    public Linestring nullLinestring(){
        return new Linestring();
    }

    public Linestring listLinestring(){
        Point a = pointA();
        Point b = pointB();
        ArrayList<Point> listeP = new ArrayList<>();
        listeP.add(a);
        listeP.add(b);
        return new Linestring(listeP);

    }

    //Envelope
    public Envelope nullEnvelope(){
        return new Envelope();
    }

}
