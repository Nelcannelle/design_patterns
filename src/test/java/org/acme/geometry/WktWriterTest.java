package org.acme.geometry;

import org.junit.Assert;
import org.junit.Test;

public class WktWriterTest {
    TestGeometryFactory tgf = new TestGeometryFactory();

    @Test
    public void testWriterNullPoint() {
        Geometry g = tgf.nullPoint();
        WktWriter writer = new WktWriter();
        Assert.assertEquals("POINT EMPTY", writer.writer(g));
    }
    @Test
    public void testWriterNullPLinestring() {
        Geometry g = tgf.nullLinestring();
        WktWriter writer = new WktWriter();
        Assert.assertEquals("LINESTRING EMPTY", writer.writer(g));
    }

    @Test
    public void testWriterPoint() {
        Geometry g = tgf.pointA();
        WktWriter writer = new WktWriter();
        Assert.assertEquals("POINT(2.0 2.0)", writer.writer(g));
    }

    @Test
    public void testWriterLinestring() {
        Geometry g = tgf.listLinestring();
        WktWriter writer = new WktWriter();
        Assert.assertEquals("LINESTRING(2.0 2.0,17.0 17.0)", writer.writer(g));
    }


}
